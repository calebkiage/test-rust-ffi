#include <stdio.h>
#include <gcrypt.h>

#define NEED_LIBGCRYPT_VERSION "1.8.0"

int main() {
    gcry_error_t err = gcry_control(GCRYCTL_SET_VERBOSITY, GCRY_LOG_DEBUG);
    /* Version check should be the very first call because it
     makes sure that important subsystems are initialized.
     #define NEED_LIBGCRYPT_VERSION to the minimum required version. */
    if (!gcry_check_version(NEED_LIBGCRYPT_VERSION)) {
        fprintf(stderr, "libgcrypt is too old (need %s, have %s)\n",
                NEED_LIBGCRYPT_VERSION, gcry_check_version(NULL));
        exit(2);
    }

    /* Disable secure memory.  */
    gcry_control(GCRYCTL_DISABLE_SECMEM, 0);

    /* ... If required, other initialization goes here.  */

    /* Tell Libgcrypt that initialization has completed. */
    gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);

    if (!gcry_control(GCRYCTL_INITIALIZATION_FINISHED_P)) {
        fputs("libgcrypt has not been initialized\n", stderr);
        abort();
    } else {
        printf("Loaded gcrypt");
    }

    return 0;
}