extern crate corelib;

use corelib::gcrypt;

fn main() {
    let res = gcrypt::initialize();
    if res.is_err() {
        panic!(format!(
            "Couldn't initialize gcrypt: {}",
            res.err().unwrap()
        ));
    }

    let version = gcrypt::get_version();
    let orig_bytes = "test".as_bytes();
    println!("{:?}", orig_bytes);
    let result = gcrypt::encrypt("", orig_bytes);
    if result.is_ok() {
        let res = result.unwrap();
        println!("{:?}", &res[144..160]);
        let decrypt_res = gcrypt::decrypt("", res.as_slice());

        if decrypt_res.is_ok() {
            let res2 = decrypt_res.unwrap();
            println!("{:?}", res2);
            println!("{}", String::from_utf8_lossy(res2.as_slice()));
        }
    } else {
        eprintln!("{}", result.err().unwrap())
    }

    println!("Gcrypt version: {}", version);
}
