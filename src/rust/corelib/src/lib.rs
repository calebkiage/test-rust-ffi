extern crate cty;
extern crate libgcrypt_sys;
extern crate libgpg_error_sys;

pub mod gcrypt {
    use std::ffi::{CStr, CString};

    use libgcrypt_sys::gcrypt;

    const NEED_LIBGCRYPT_VERSION: &str = "1.6.0";
    const AES256_KEY_SIZE: usize = 32;
    const AES256_BLOCK_SIZE: usize = 16;
    const HMAC_KEY_SIZE: usize = 64;
    const KDF_ITERATIONS: u64 = 50000;
    const KDF_SALT_SIZE: usize = 128;
    const KDF_KEY_SIZE: usize = AES256_KEY_SIZE + HMAC_KEY_SIZE;

    pub fn get_version() -> String {
        let res = verify_initialized();
        res.unwrap();

        unsafe {
            return CStr::from_ptr(gcrypt::gcry_check_version(std::ptr::null()))
                .to_string_lossy()
                .into_owned();
        }
    }

    pub fn decrypt(password: &str, content: &[u8]) -> Result<Vec<u8>, String> {
        verify_initialized().unwrap();

        unsafe {
            let hmac_len = gcrypt::gcry_mac_get_algo_maclen(
                gcrypt::gcry_mac_algos::GCRY_MAC_HMAC_SHA512 as i32,
            ) as usize;

            // Check the content is not too short.
            let metadata_length =
                KDF_SALT_SIZE + AES256_BLOCK_SIZE + 1 + hmac_len + AES256_BLOCK_SIZE;
            if content.len() < metadata_length {
                return Err(format!(
                    "The input data is too short. Please pass in encrypted data."
                ));
            }

            // Pack data format: salt::IV::padding::ciphertext::HMAC where "::" denotes concatenation
            let iv_start_index = KDF_SALT_SIZE;
            let padding_start_index = KDF_SALT_SIZE + AES256_BLOCK_SIZE;
            let hmac_start_index = content.len() - hmac_len;

            let kdf_salt_buffer = &content[0..iv_start_index];
            let iv_buffer = &content[iv_start_index..padding_start_index];
            let padding = &content[padding_start_index];
            let cipher_text_buffer = &content[padding_start_index + 1..hmac_start_index];
            let hmac = &content[hmac_start_index..content.len()];

            let cipher_text_len = hmac_start_index - (padding_start_index + 1);
            let message_len = content.len() - hmac_len;

            // Key derivation: PBKDF2 using SHA512 w/ KDF_SALT_SIZE byte salt over KDF_ITERATIONS
            // iterations into a KDF_KEY_SIZE byte key
            let kdf_key_buffer: [u8; KDF_KEY_SIZE] = [0; KDF_KEY_SIZE];
            let derive_res = gcrypt::gcry_kdf_derive(
                password.as_ptr() as *const _,
                (&password).len(),
                gcrypt::gcry_kdf_algos::GCRY_KDF_PBKDF2 as i32,
                gcrypt::gcry_md_algos::GCRY_MD_SHA512 as i32,
                kdf_salt_buffer.as_ptr() as *const _,
                KDF_SALT_SIZE,
                KDF_ITERATIONS,
                KDF_KEY_SIZE,
                kdf_key_buffer.as_ptr() as *mut _,
            );
            validate_gcrypt_result(derive_res, "kdf_derive").unwrap();

            let aes_key = &kdf_key_buffer[0..AES256_KEY_SIZE];
            let hmac_key = &kdf_key_buffer[&kdf_key_buffer.len() - HMAC_KEY_SIZE..KDF_KEY_SIZE];

            // Begin HMAC verification
            let mut mac_handle: gcrypt::gcry_mac_hd_t = std::ptr::null_mut();
            let mac_handle_ptr = &mut mac_handle as *mut _;
            let mac_ctx_ptr = std::ptr::null_mut();
            let mac_open_res = gcrypt::gcry_mac_open(
                mac_handle_ptr,
                gcrypt::gcry_mac_algos::GCRY_MAC_HMAC_SHA512 as i32,
                0,
                mac_ctx_ptr,
            );
            validate_gcrypt_result(mac_open_res, "mac_open").unwrap();

            let mac_setkey_res =
                gcrypt::gcry_mac_setkey(mac_handle, hmac_key.as_ptr() as *const _, HMAC_KEY_SIZE);
            validate_gcrypt_result(mac_setkey_res, "mac_setkey").unwrap();

            let mac_write_res =
                gcrypt::gcry_mac_write(mac_handle, content.as_ptr() as *mut _, message_len);
            validate_gcrypt_result(mac_write_res, "mac_write").unwrap();

            // Verify HMAC
            let mac_verify_res =
                gcrypt::gcry_mac_verify(mac_handle, hmac.as_ptr() as *const _, hmac_len);
            let validation = validate_gcrypt_result(mac_verify_res, "mac_verify");
            if validation.is_err() {
                return Err(format!(
                    "Error verifying message signature. You may have mistyped your password: {}",
                    validation.err().unwrap()
                ));
            }

            // Begin decryption
            let mut handle: gcrypt::gcry_cipher_hd_t = std::ptr::null_mut();
            let init_cipher_res = init_cypher(
                &mut handle,
                aes_key.as_ptr() as *const _,
                iv_buffer.as_ptr() as *const _,
            );
            if init_cipher_res.is_err() {
                return Err(format!(
                    "Could not initialize the cipher: {}",
                    init_cipher_res.err().unwrap()
                ));
            }

            let output_buffer: Vec<u8> = vec![0; cipher_text_len];
            let decrypt_res = gcrypt::gcry_cipher_decrypt(
                handle,
                output_buffer.as_ptr() as *mut _,
                cipher_text_len,
                cipher_text_buffer.as_ptr() as *const _,
                cipher_text_len,
            );
            validate_gcrypt_result(decrypt_res, "cipher_encrypt").unwrap();

            gcrypt::gcry_mac_close(mac_handle);
            gcrypt::gcry_cipher_close(handle);
            let padding_size = *padding as usize;
            return Ok(output_buffer[0..16 - padding_size].to_vec());
        }
    }

    pub fn encrypt(password: &str, content: &[u8]) -> Result<Vec<u8>, String> {
        verify_initialized().unwrap();

        unsafe {
            let mut kdf_salt_buffer: [u8; KDF_SALT_SIZE] = [0; KDF_SALT_SIZE];
            // Generate a KDF_SALT_SIZE byte salt in preparation for key derivation
            gcrypt::gcry_create_nonce((&mut kdf_salt_buffer).as_mut_ptr() as *mut _, KDF_SALT_SIZE);

            let kdf_key_buffer: [u8; KDF_KEY_SIZE] = [0; KDF_KEY_SIZE];

            // Key derivation: PBKDF2 using SHA512 w/ KDF_SALT_SIZE byte salt over KDF_ITERATIONS
            // iterations into a KDF_KEY_SIZE byte key
            let derive_res = gcrypt::gcry_kdf_derive(
                password.as_ptr() as *const _,
                (&password).len(),
                gcrypt::gcry_kdf_algos::GCRY_KDF_PBKDF2 as i32,
                gcrypt::gcry_md_algos::GCRY_MD_SHA512 as i32,
                kdf_salt_buffer.as_ptr() as *mut _,
                KDF_SALT_SIZE,
                KDF_ITERATIONS,
                KDF_KEY_SIZE,
                kdf_key_buffer.as_ptr() as *mut _,
            );
            validate_gcrypt_result(derive_res, "kdf_derive").unwrap();

            let hmac_key = &kdf_key_buffer[&kdf_key_buffer.len() - HMAC_KEY_SIZE..KDF_KEY_SIZE];

            // Generate the initialization vector.
            let iv_buffer: [u8; AES256_BLOCK_SIZE] = [0; AES256_BLOCK_SIZE];
            gcrypt::gcry_create_nonce(iv_buffer.as_ptr() as *mut _, AES256_BLOCK_SIZE);

            let mut handle: gcrypt::gcry_cipher_hd_t = std::ptr::null_mut();
            let init_cipher_res = init_cypher(
                &mut handle,
                kdf_key_buffer.as_ptr() as *mut _,
                iv_buffer.as_ptr() as *mut _,
            );
            if init_cipher_res.is_err() {
                return Err(format!(
                    "Could not initialize the cipher: {}",
                    init_cipher_res.err().unwrap()
                ));
            }

            let length = content.len();

            let blocks_required = (length as f64 / AES256_BLOCK_SIZE as f64).ceil() as usize;
            let content_size = blocks_required * AES256_BLOCK_SIZE;
            let mut cipher_text: Vec<u8> = vec![0; content_size];
            // Create padded input.
            for i in 0..content_size {
                let mut val: u8 = 0;
                if content.get(i).is_some() {
                    val = content[i];
                }
                cipher_text[i] = val
            }
            let output_buffer: Vec<u8> = vec![0; content_size];
            let encrypt_res = gcrypt::gcry_cipher_encrypt(
                handle,
                output_buffer.as_ptr() as *mut _,
                content_size,
                cipher_text.as_ptr() as *const _,
                content_size,
            );
            validate_gcrypt_result(encrypt_res, "cipher_encrypt").unwrap();

            // Compute and allocate space required for packed data
            let mut hmac_len = gcrypt::gcry_mac_get_algo_maclen(
                gcrypt::gcry_mac_algos::GCRY_MAC_HMAC_SHA512 as i32,
            ) as usize;

            let mut packed_data = vec![0; 0];
            // Pack data before writing: salt::IV::padding::ciphertext::HMAC where "::" denotes concatenation
            packed_data.extend(kdf_salt_buffer.iter());
            packed_data.extend(iv_buffer.iter());

            // Because of a block size of 16 bytes, we'll only ever add upto 16 bytes.
            // A u8 is sufficient to store that information.
            let padding: u8 = (content_size - length) as u8;
            packed_data.extend([padding].iter());
            packed_data.extend(output_buffer.iter());

            // Begin HMAC computation
            let mut mac_handle: gcrypt::gcry_mac_hd_t = std::ptr::null_mut();
            let mac_open_res = gcrypt::gcry_mac_open(
                &mut mac_handle as *mut _,
                gcrypt::gcry_mac_algos::GCRY_MAC_HMAC_SHA512 as i32,
                0,
                std::ptr::null_mut(),
            );
            validate_gcrypt_result(mac_open_res, "mac_open").unwrap();

            // Set HMAC key
            let mac_setkey_res =
                gcrypt::gcry_mac_setkey(mac_handle, hmac_key.as_ptr() as _, HMAC_KEY_SIZE);
            validate_gcrypt_result(mac_setkey_res, "mac_setkey").unwrap();

            let mac_write_res = gcrypt::gcry_mac_write(
                mac_handle,
                packed_data.as_ptr() as *mut _,
                packed_data.len(),
            );
            validate_gcrypt_result(mac_write_res, "mac_write").unwrap();

            let hmac_buffer: Vec<u8> = vec![0; hmac_len];
            let mac_read_res = gcrypt::gcry_mac_read(
                mac_handle,
                hmac_buffer.as_ptr() as *mut _,
                &mut hmac_len as *mut _,
            );
            validate_gcrypt_result(mac_read_res, "mac_read").unwrap();

            packed_data.extend(hmac_buffer.iter());

            gcrypt::gcry_mac_close(mac_handle);
            gcrypt::gcry_cipher_close(handle);
            return Ok(packed_data);
        }
    }

    fn init_cypher(
        handle: &mut gcrypt::gcry_cipher_hd_t,
        key: *const cty::c_void,
        init_vector: *const cty::c_void,
    ) -> Result<(), String> {
        unsafe {
            let raw: *mut gcrypt::gcry_cipher_hd_t = handle;
            // 256-bit AES using cipher-block chaining; with ciphertext stealing, no manual padding is required
            let open_res = gcrypt::gcry_cipher_open(
                raw,
                gcrypt::gcry_cipher_algos::GCRY_CIPHER_AES256 as i32,
                gcrypt::gcry_cipher_modes::GCRY_CIPHER_MODE_CBC as i32,
                gcrypt::gcry_cipher_flags::GCRY_CIPHER_CBC_CTS as u32,
            );
            validate_gcrypt_result(open_res, "cipher_open").unwrap();

            let set_key_res = gcrypt::gcry_cipher_setkey(*handle, key, AES256_KEY_SIZE);
            validate_gcrypt_result(set_key_res, "cipher_setkey").unwrap();

            let set_iv_res = gcrypt::gcry_cipher_setiv(*handle, init_vector, AES256_BLOCK_SIZE);
            validate_gcrypt_result(set_iv_res, "cipher_setiv").unwrap();

            return Ok(());
        }
    }

    pub fn initialize() -> Result<(), String> {
        unsafe {
            if is_initialized() {
                return Ok(());
            }

            let minimum_version: CString =
                CString::new(NEED_LIBGCRYPT_VERSION).expect("Failed to create string");
            gcrypt::gcry_control(
                gcrypt::gcry_ctl_cmds::GCRYCTL_SET_VERBOSITY,
                gcrypt::gcry_log_levels::GCRY_LOG_DEBUG,
            );

            // Version check should be the very first call because it
            // makes sure that important subsystems are initialized.
            // #define NEED_LIBGCRYPT_VERSION to the minimum required version. */
            let res = gcrypt::gcry_check_version(minimum_version.as_ptr());
            if res.is_null() {
                return Err(format!(
                    "libgcrypt version error. Expected at least {} but found {}",
                    minimum_version.to_string_lossy(),
                    CStr::from_ptr(gcrypt::gcry_check_version(std::ptr::null())).to_string_lossy() // 0 means NULL
                ));
            }

            // We don't want to see any warnings, e.g. because we have not yet
            // parsed program options which might be used to suppress such
            // warnings.
            let suspend_sec_mem_warn_res =
                gcrypt::gcry_control(gcrypt::gcry_ctl_cmds::GCRYCTL_SUSPEND_SECMEM_WARN);
            let res = validate_gcrypt_result(
                suspend_sec_mem_warn_res,
                "gcry_control-GCRYCTL_SUSPEND_SECMEM_WARN",
            );
            if res.is_err() {
                return Err(format!("Initialization failure: {}", res.err().unwrap()));
            }

            /* ... If required, other initialization goes here.  Note that the
            process might still be running with increased privileges and that
            the secure memory has not been initialized.  */

            /* Allocate a pool of 16k secure memory.  This makes the secure memory
            available and also drops privileges where needed.  Note that by
            using functions like gcry_xmalloc_secure and gcry_mpi_snew Libgcrypt
            may expand the secure memory pool with memory which lacks the
            property of not being swapped out to disk.   */
            let init_sec_mem_res =
                gcrypt::gcry_control(gcrypt::gcry_ctl_cmds::GCRYCTL_INIT_SECMEM, 16384);
            let init_res =
                validate_gcrypt_result(init_sec_mem_res, "gcry_control-GCRYCTL_INIT_SECMEM");
            if init_res.is_err() {
                return Err(format!(
                    "Initialization failure: {}",
                    init_res.err().unwrap()
                ));
            }

            /* It is now okay to let Libgcrypt complain when there was/is
            a problem with the secure memory. */
            let resume_sec_mem_warn_res =
                gcrypt::gcry_control(gcrypt::gcry_ctl_cmds::GCRYCTL_RESUME_SECMEM_WARN);
            let res = validate_gcrypt_result(
                resume_sec_mem_warn_res,
                "gcry_control-GCRYCTL_RESUME_SECMEM_WARN",
            );
            if res.is_err() {
                return Err(format!("Initialization failure: {}", res.err().unwrap()));
            }

            /* ... If required, other initialization goes here.  */
            let enable_m_guard_res =
                gcrypt::gcry_control(gcrypt::gcry_ctl_cmds::GCRYCTL_ENABLE_M_GUARD, 0);
            let m_guard_res =
                validate_gcrypt_result(enable_m_guard_res, "gcry_control-GCRYCTL_ENABLE_M_GUARD");
            if m_guard_res.is_err() {
                return Err(format!(
                    "Initialization failure: {}",
                    m_guard_res.err().unwrap()
                ));
            }

            /* Tell Libgcrypt that initialization has completed. */
            let initialization_finished_res =
                gcrypt::gcry_control(gcrypt::gcry_ctl_cmds::GCRYCTL_INITIALIZATION_FINISHED, 0);
            let set_init_finished_res = validate_gcrypt_result(
                initialization_finished_res,
                "gcry_control-GCRYCTL_INITIALIZATION_FINISHED",
            );
            if set_init_finished_res.is_err() {
                return Err(format!(
                    "Initialization failure: {}",
                    set_init_finished_res.err().unwrap()
                ));
            }
        }

        return Ok(());
    }

    fn is_init_finished() -> gcrypt::gcry_error_t {
        unsafe { gcrypt::gcry_control(gcrypt::gcry_ctl_cmds::GCRYCTL_INITIALIZATION_FINISHED_P) }
    }

    pub fn is_initialized() -> bool {
        let init_finished = is_init_finished();
        return init_finished != 0;
    }

    fn validate_gcrypt_result(result: gcrypt::gcry_error_t, prefix: &str) -> Result<(), String> {
        if result != 0 {
            unsafe {
                return Err(format!(
                    "{}: {}/{}",
                    prefix,
                    CStr::from_ptr(gcrypt::gcry_strsource(result)).to_string_lossy(),
                    CStr::from_ptr(gcrypt::gcry_strerror(result)).to_string_lossy()
                ));
            }
        }

        return Ok(());
    }

    fn verify_initialized() -> Result<(), String> {
        let result = is_init_finished();
        if result == 0 {
            unsafe {
                return Err(format!(
                    //"libgcrypt has not been initialized",
                    "libgcrypt has not been initialized: {}/{}",
                    CStr::from_ptr(gcrypt::gcry_strsource(result)).to_string_lossy(),
                    CStr::from_ptr(gcrypt::gcry_strerror(result)).to_string_lossy()
                ));
            }
        }

        return Ok(());
    }
}

#[cfg(test)]
mod tests {
    #[cfg(test)]
    mod get_version {
        use super::super::gcrypt;

        #[test]
        fn gets_version() {
            gcrypt::initialize().unwrap();
            let result = gcrypt::get_version();
            assert!(!result.trim().is_empty());
        }
    }

    mod initialize {
        use super::super::gcrypt;

        #[test]
        fn initializes_library() {
            let result = gcrypt::initialize();
            let initialized = gcrypt::is_initialized();

            assert!(result.is_ok());
            assert!(initialized);
        }
    }

    mod encrypt {
        use super::super::gcrypt;

        #[test]
        fn encrypts_empty_content() {
            gcrypt::initialize().unwrap();
            let data: [u8; 0] = [];
            let password = "";

            let result = gcrypt::encrypt(password, &data);

            assert!(result.is_ok());
            let encrypted = result.unwrap();
            assert_ne!(encrypted.as_slice(), &data);
        }

        #[test]
        fn encrypts_short_length_content() {
            gcrypt::initialize().unwrap();
            let data: &[u8] = "aba".as_bytes();
            let password = "";

            let result = gcrypt::encrypt(password, data);

            assert!(result.is_ok());
            let encrypted = result.unwrap();
            assert_ne!(encrypted.as_slice(), data);
        }
    }

    mod decrypt {
        use super::super::gcrypt;

        #[test]
        fn decrypts_encrypted_empty_content() {
            gcrypt::initialize().unwrap();
            let data: [u8; 1] = [0];
            let password = "";

            let result = gcrypt::encrypt(password, &data);
            assert!(result.is_ok());
            let encrypted = result.unwrap();
            let decrypted_res = gcrypt::decrypt(password, encrypted.as_slice());

            assert!(decrypted_res.is_ok());
            let decrypted = decrypted_res.unwrap();
            assert_eq!(data, decrypted.as_slice());
        }

        #[test]
        fn fails_decryption_on_incorrect_password() {
            gcrypt::initialize().unwrap();
            let data = "test".as_bytes();
            let password = "";

            let result = gcrypt::encrypt(password, &data);
            assert!(result.is_ok());
            let encrypted = result.unwrap();
            let decrypted_res = gcrypt::decrypt("wrong", encrypted.as_slice());

            assert!(decrypted_res.is_err());
            let decrypted = decrypted_res.unwrap_err();
            assert!(!decrypted.is_empty());
        }
    }
}
