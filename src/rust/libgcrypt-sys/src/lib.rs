#![deny(warnings)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused)]

//mod bindings;

pub mod gcrypt {
    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
    //pub use super::bindings::*;
}

#[cfg(test)]
mod tests {
    use std::ffi::{CStr, CString};

    use super::gcrypt;

    #[test]
    fn links_gcrypt() {
        unsafe {
            let v_string: CString = CString::new("1.6.0").expect("Failed to create string");
            let expected_version: String = String::from("1.8.5");
            let result = gcrypt::gcry_check_version(v_string.as_ptr());
            debug_assert!(!(result.is_null()));
            let v: String = CStr::from_ptr(result).to_string_lossy().into_owned();
            println!("{}", v);
            assert_eq!(v, expected_version);
        }
    }
}
