If gpg-error is not installed on your system:

1. Download and build gpg-error using the below commands:
   ```shell script
   $ ./autogen.sh
   $ ./configure --disable-doc --prefix=INSTALL_DIR --host={} --with-pic --enable-shared --disable-dependency-tracking --disable-static
   $ make
   $ make install
   ```
   > `INSTALL_DIR` is an absolute path to the directory make install will copy the built binaries. 

2. Build the project with the `LIBGPG_ERROR_PREFIX` environment variable being the `INSTALL_DIR` you specified above.
   ```shell script
   $ LIBGPG_ERROR_PREFIX=INSTALL_DIR cargo build
   ```