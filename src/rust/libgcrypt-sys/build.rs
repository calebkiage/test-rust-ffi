extern crate bindgen;

use std::env;

use std::path::PathBuf;
use std::process::Command;

fn gnu_target(target: &str) -> String {
    match target {
        "i686-pc-windows-gnu" => "i686-w64-mingw32".to_string(),
        "x86_64-pc-windows-gnu" => "x86_64-w64-mingw32".to_string(),
        s if s.starts_with("i686-unknown") || s.starts_with("x86_64-unknown") => {
            s.replacen("unknown", "pc", 1)
        }
        s => s.to_string(),
    }
}

fn main() {
    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.h");

    // Tell cargo to tell rustc to link the library.
    println!("cargo:rustc-link-lib=gcrypt");

    let output_dir = &env::var("OUT_DIR").unwrap();
    let target = gnu_target(&env::var("TARGET").unwrap());
    let gcrypt_prefix_dir: String;
    let building_for_current_target = &env::var("TARGET").unwrap() == &env::var("HOST").unwrap();

    // Check if system has gcrypt installed
    let gcrypt_prefix = Command::new("sh")
        .arg("-c")
        .arg("libgcrypt-config --prefix")
        .output();

    if building_for_current_target && gcrypt_prefix.is_ok() {
        // We have gcrypt on the system
        gcrypt_prefix_dir = String::from_utf8(gcrypt_prefix.unwrap().stdout)
            .unwrap()
            .trim()
            .to_string();
    } else {
        let gcrypt_output_dir = PathBuf::from(output_dir).join("gcrypt");
        gcrypt_prefix_dir = (&gcrypt_output_dir).to_string_lossy().into_owned();
        build_gpg_error(&gcrypt_prefix_dir, &target);
        build_gcrypt(&gcrypt_prefix_dir, &target, &gcrypt_prefix_dir);

        println!("cargo:rustc-link-search=native={}/lib", &gcrypt_prefix_dir);
    }

    // Get cflags
    let cflags_out = Command::new("sh")
        .arg("-c")
        .arg(format!(
            "{}/bin/libgcrypt-config --cflags",
            &gcrypt_prefix_dir
        ))
        .output()
        .unwrap();

    let cflags = String::from_utf8(cflags_out.stdout)
        .unwrap()
        .trim() // Breaks bindgen if cflags is a string with only whitespace characters i.e. "\n"
        .to_owned();

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .generate_comments(true)
        .ctypes_prefix("cty")
        .clang_arg(&cflags)
        .use_core()
        .whitelist_type("gcry_.*")
        .whitelist_function("gcry_.*")
        .whitelist_var("GCRY.*")
        .default_enum_style(bindgen::EnumVariation::ModuleConsts)
        .disable_name_namespacing()
        .time_phases(true)
        .layout_tests(true)
        .array_pointers_in_arguments(true)
        .generate()
        .expect("Unable to generate bindings");

    //let out_path = PathBuf::from("src").join("bindings.rs");
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap()).join("bindings.rs");

    bindings
        .write_to_file(&out_path)
        .expect("Couldn't write bindings!");
}

fn build_gpg_error(output_dir: &str, target: &str) {
    let project_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    // Build gpg-error
    let libgpg_error_dir: PathBuf = PathBuf::from(&project_dir).join("libgpg-error");

    // Don't use namespace variable (builtin variable since v5.0)
    Command::new("sh")
        .arg("-c")
        .arg("sed -i -e 's/namespace/varerrno/g' src/{Makefile.am,mkstrtable.awk}")
        .current_dir(&libgpg_error_dir)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    Command::new("sh")
        .arg("-c")
        .arg("./autogen.sh")
        .current_dir(&libgpg_error_dir)
        .status()
        .unwrap();

    Command::new("sh")
        .arg("-c")
        .arg(format!(
            "./configure \
             --disable-doc \
             --prefix={} \
             --host={} \
             --with-pic \
             --enable-shared \
             --disable-dependency-tracking \
             --disable-static",
            &output_dir, &target
        ))
        .current_dir(&libgpg_error_dir)
        .status()
        .unwrap();

    Command::new("sh")
        .arg("-c")
        .arg("make")
        .current_dir(&libgpg_error_dir)
        .status()
        .unwrap();

    Command::new("sh")
        .arg("-c")
        .arg("make install")
        .current_dir(&libgpg_error_dir)
        .status()
        .unwrap();
}

fn build_gcrypt(output_dir: &str, target: &str, gpg_error_dir: &str) {
    let project_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    // Build gcrypt
    let gcrypt_dir: PathBuf = PathBuf::from(&project_dir).join("libgcrypt");

    Command::new("sh")
        .arg("-c")
        .arg(format!("{}/autogen.sh", &gcrypt_dir.to_string_lossy()))
        .current_dir(gcrypt_dir.join("."))
        .status()
        .unwrap();

    let configure_cmd = format!(
        "{}/configure \
         --disable-doc \
         --prefix={} \
         --host={} \
         --with-pic \
         --with-libgpg-error-prefix={} \
         --enable-shared \
         --disable-static",
        &gcrypt_dir.to_string_lossy(),
        &output_dir,
        &target,
        &gpg_error_dir
    );

    Command::new("sh")
        .arg("-c")
        .arg(&configure_cmd)
        .current_dir(&gcrypt_dir)
        .status()
        .unwrap();

    Command::new("sh")
        .arg("-c")
        .arg("make")
        .current_dir(&gcrypt_dir)
        .status()
        .unwrap();

    Command::new("sh")
        .arg("-c")
        .arg("make install")
        .current_dir(&gcrypt_dir)
        .status()
        .unwrap();
}
